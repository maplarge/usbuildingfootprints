#! /usr/env python

import os
import json
import wget
import requests

# Globals
server      = "https://ml-local.maplarge.net"
credentials = { 'mluser': 'root_user@ml.com', 'mlpass': 'map!large123' }

# States
jsonStates =  {"AL":"Alabama","AK":"Alaska","AZ":"Arizona","AR":"Arkansas","CA":"California","CO":"Colorado","CT":"Connecticut","DE":"Delaware","DC":"District of Columbia","FL":"Florida","GA":"Georgia","HI":"Hawaii","ID":"Idaho","IL":"Illinois","IN":"Indiana","IA":"Iowa","KS":"Kansas","KY":"Kentucky","LA":"Louisiana","ME":"Maine","MD":"Maryland","MA":"Massachusetts","MI":"Michigan","MN":"Minnesota","MS":"Mississippi","MO":"Missouri","MT":"Montana","NE":"Nebraska","NV":"Nevada","NH":"New Hampshire","NJ":"New Jersey","NM":"New Mexico","NY":"New York","NC":"North Carolina","ND":"North Dakota","OH":"Ohio","OK":"Oklahoma","OR":"Oregon","PA":"Pennsylvania","RI":"Rhode Island","SC":"South Carolina","SD":"South Dakota","TN":"Tennessee","TX":"Texas","UT":"Utah","VT":"Vermont","VA":"Virginia","WA":"Washington","WV":"West Virginia","WI":"Wisconsin","WY":"Wyoming"}

# These files take quite a wile for the server to process. Sometimes the script hangs or has to be restarted
# Grab the tables that are in the server already so that they can be skipped on restarts
url = server + \
      "/Remote/GetAllTables?account=msftStructures"
try:
    r = requests.get(url, headers=credentials)
    r.raise_for_status()
except requests.exceptions.HTTPError as err:
    raise(err)

uploadedTables = r.json()
#print(json.dumps(uploadedTables, indent=2))

# Create a list of already uploaded states
# "id": "msftStructures/Arkansas/636755622074651194",
tables = []
for table in uploadedTables["tables"]:
    for k, v in table.items():
        if k == "id":
            tables.append(v.split('/')[1])

# Load the files directly from the Microsoft repo into local MapLarge Server
# Or do this r = requests.post(url, files={'myfile.csv': open('myfile.csv', 'rb')})
for key, value in jsonStates.items():
  name = value.replace(' ', '')
  filename = name + ".zip"
  exists = os.path.isfile(filename)
  
  # Do we already have this file?
  if exists:
    # Test to see if the table has already been updated
    if name in tables:
        print("\nSkipping:", name)
        continue
    else:
        print("\nUploading:", value)

    # Set up the API request
    url = server + \
          "/Remote/CreateTableWithFilesSynchronous?" + \
          "&account=msftStructures&tablename=" + name
    
    # Open the file that we are going to send
    filestream = open(filename, 'rb')

    # POST it to the server
    try:
        r = requests.post(url, headers=credentials, files={filename: filestream})
        r.raise_for_status()
    except requests.exceptions.HTTPError as err:
        raise(err)
    finally:
        filestream.close()

    print(json.dumps(r.json(), indent=2))

  # Download the file (TODO: and then upload it?)  
  else:
    print("\nDownloading:", value)
    url = "https://usbuildingdata.blob.core.windows.net/usbuildings-v1-1/" + name + ".zip"
    wget.download(url) 
 
