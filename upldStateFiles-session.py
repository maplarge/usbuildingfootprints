#! /usr/env python

import os
import json
import wget
import requests

# Globals
server      = "https://qa-imports.maplarge.com/"
credentials = { 'mluser': 'root_user@ml.com', 'mlpass': 'qa!pass123' }
dnldLocal   = False

# States
jsonStates =  {"AL":"Alabama","AK":"Alaska","AZ":"Arizona","AR":"Arkansas","CA":"California","CO":"Colorado","CT":"Connecticut","DE":"Delaware","DC":"District of Columbia","FL":"Florida","GA":"Georgia","HI":"Hawaii","ID":"Idaho","IL":"Illinois","IN":"Indiana","IA":"Iowa","KS":"Kansas","KY":"Kentucky","LA":"Louisiana","ME":"Maine","MD":"Maryland","MA":"Massachusetts","MI":"Michigan","MN":"Minnesota","MS":"Mississippi","MO":"Missouri","MT":"Montana","NE":"Nebraska","NV":"Nevada","NH":"New Hampshire","NJ":"New Jersey","NM":"New Mexico","NY":"New York","NC":"North Carolina","ND":"North Dakota","OH":"Ohio","OK":"Oklahoma","OR":"Oregon","PA":"Pennsylvania","RI":"Rhode Island","SC":"South Carolina","SD":"South Dakota","TN":"Tennessee","TX":"Texas","UT":"Utah","VT":"Vermont","VA":"Virginia","WA":"Washington","WV":"West Virginia","WI":"Wisconsin","WY":"Wyoming"}

# Handle the callback function
def print_response (r, *args, **kwargs):
    if "tables" in r.json():
        for t in r.json()["tables"]:
            if "name" in t:
                print("Uploaded:", t["name"], '{:,}'.format(t["rowCount"]), t["created"])
            else:
                print ("Table:", '{:<55}'.format(t["id"]), '{:>10,}'.format(t["rowcount"]), t["created"])
    else: 
        print(json.dumps(r.json(), indent=2))

# Open a session with the server
s = requests.Session()
s.headers.update(credentials)
s.hooks['response'].append(print_response)

# These files take quite a wile for the server to process. Sometimes the script hangs or has to be restarted
# Grab the tables that are in the server already so that they can be skipped on restarts
url = server + \
      "/Remote/GetAllTables?account=msftStructures"
try:
    r = s.get(url, headers=credentials)
    r.raise_for_status()
except requests.exceptions.HTTPError as err:
    raise(err)

uploadedTables = r.json()
#print(json.dumps(uploadedTables, indent=2))

# Create a list of already uploaded states
# "id": "msftStructures/Arkansas/636755622074651194",
tables = []
for table in uploadedTables["tables"]:
    for k, v in table.items():
        if k == "id":
            tables.append(v.split('/')[1])

# Load the files into a MapLarge Server
for key, value in jsonStates.items():
  name = value.replace(' ', '')
  filename = ".\\data\\" + name + ".zip"
  exists = os.path.isfile(filename)
  
  # Do we already have this file?
  if exists:
    # Test to see if the table has already been updated
    if name in tables:
        print("\nSkipping:", value)
        continue
    else:
        print("\nUploading:", value)

    if dnldLocal == True:
        # Set up the API request - this downloads the files locally first and then pushes then to a server
        url = server + \
            "/Remote/CreateTableWithFilesSynchronous?" + \
            "&account=msftStructures&tablename=" + name
        
        # Open the file that we are going to send
        filestream = open(filename, 'rb')

        # POST the file to the server
        try:
            r = s.post(url, files={filename: filestream})
            r.raise_for_status()
        except requests.exceptions.HTTPError as err:
            raise(err)
        finally:
            filestream.close()

    else:
        # This version has the server load the data directly from the Microsoft repository (server to server)
        url = server + \
            "/Remote/CreateTableSynchronous?" + \
            "&account=msftStructures&tablename=" + name + \
            "&fileurl=https://usbuildingdata.blob.core.windows.net/usbuildings-v1-1/" + name + ".zip"
        
        # POST the file url to the server (server to server)
        try:
            r = s.post(url)
            r.raise_for_status()
        except requests.exceptions.HTTPError as err:
            raise(err)

  # Download the file (TODO: refactor this to use the server to server logic above)  
  else:
    print("\nDownloading:", value)
    url = "https://usbuildingdata.blob.core.windows.net/usbuildings-v1-1/" + name + ".zip"
    wget.download(url) 

